import React, { Component } from 'react';

import Quill from 'quill';

import Delta from 'quill-delta';

import GoogleFont from './GoogleFont';

import { whitelist, importFont, addStyle, findFont } from './helper';

import kebabCase from 'lodash/kebabCase';

const Font = Quill.import('formats/font');

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            quill: null,
            modal: false,
            range: {
                index: 0,
                length: 0
            }
        };

        this._handleSave = this._handleSave.bind(this);

        this.setFont    = this.setFont.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.openModal  = this.openModal.bind(this);

        this._handleSelectionChange = this._handleSelectionChange.bind(this);

    }

    componentDidMount() {

        const text = localStorage.getItem('text');

        const delta = text ? JSON.parse(text) : new Delta();

        let quill = new Quill('#editor', {
            modules: {
                toolbar: {
                    container: "#toolbar",
                    handlers: {
                        "google-font": function (value) {
                            console.log(value);
                        }
                    }
                }
            },
            theme: 'snow'
        });

        // Add Whitelist fonts
        Font.whitelist = whitelist();
        Quill.register(Font, true);

        // Handle Selection change
        quill.on('selection-change', this._handleSelectionChange);

        // Update delta for editor
        quill.setContents(delta);

        // Add google font support
        for (let i = 0; i < delta.ops.length; i++) {
            const del = delta.ops[i];
            if (del.attributes && del.attributes.font) {
                const family = del.attributes.font;
                const font   = findFont(family);
                console.log(font);
                importFont(font.family);
                addStyle(font.family);
            }
        }

        this.setState({
            quill: quill
        });

    }

    _handleSelectionChange(range, oldRange, source) {

        if (range && range.length) {
            this.setState({
                range: range,
            });
        }
    }

    _handleSave(e) {

        e.preventDefault();
        const {quill} = this.state;
        localStorage.setItem('text', JSON.stringify(quill.getContents()));
    }

    setFont(font) {
        const {quill, range} = this.state;

        // Format Text
        quill.formatText(range.index, range.length, 'font', kebabCase(font.family.toLowerCase()));

        // Add Style font to head
        importFont(font.family);
        addStyle(font.family);
    }

    closeModal(e) {
        this.setState({
            modal: false,
            range: {
                index: 0,
                length: 0
            }
        });

    }

    openModal(e) {
        this.setState({
            modal: true,
        });
    }

    render() {

        return (
            <div className='container'>

                <h1>Quill</h1>

                <GoogleFont show={this.state.modal} closeModal={this.closeModal} setFont={this.setFont}/>

                <div id="toolbar">
                    <span className="ql-formats">
                        <select className="ql-font">
                            <option value="roboto">roboto</option>
                            <option value="dancing-script">dancing-script</option>
                        </select>
                        <button className="ql-google-font" onClick={this.openModal}>
		                    <i className="fa fa-font"></i>
		                </button>
                        <button className="ql-bold"></button>
                        <button className="ql-italic"></button>
                        <select className="ql-color">
                            <option value="red"></option>
                            <option value="green"></option>
                            <option value="blue"></option>
                            <option value="orange"></option>
                            <option value="violet"></option>
                            <option value="#d0d1d2"></option>
                        </select>
                    </span>
                </div>
                <div id="editor"></div>

                <button onClick={this._handleSave}>Save</button>

            </div>
        )

    }
}

export default App;