import data from './data.json';

import kebabCase from 'lodash/kebabCase';
import replace from 'lodash/replace';

export function whitelist() {
    const items = data.items;

    return items.map(font => {
        return `${kebabCase(font.family.toLowerCase())}`;
    });

}

export function findFont(f) {
    const items = data.items;

    const font = items.find(item => {
        return `${kebabCase(item.family.toLowerCase())}` === f;
    });

    return font;
}

export function importFont(family) {

    let head  = document.head || document.getElementsByTagName('head')[0],
        link  = document.createElement('link');
    link.type = 'text/css';
    link.rel  = 'stylesheet';
    link.href = `//fonts.googleapis.com/css?family=${replace(family, ' ', '+')}`;

    head.appendChild(link);

}

export function addStyle(family) {

    let head = document.head || document.getElementsByTagName('head')[0],
        style = document.createElement('style');
    style.type = 'text/css';
    style.innerText = `.ql-font-${kebabCase(family)} { font-family: '${family.toLowerCase()}' }`;

    head.appendChild(style);
}