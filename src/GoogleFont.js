import React, { Component } from 'react';

import 'react-quill/dist/quill.snow.css';

import data from './data.json';

import take from 'lodash/take';

// Add bootstrap
import Modal from 'react-bootstrap/lib/Modal';
import Button from 'react-bootstrap/lib/Button';

class GoogleFont extends Component {

    constructor(props) {
        super(props);

        this.state = {
            items: take(data.items, 20),
            range: this.props.range
        };

        this._onSearch = this._onSearch.bind(this);

    }

    componentWillReceiveProps(nextProps) {

        this.setState({
            range: nextProps.range,
        });


        return true;
    }

    _onSearch(e) {

        e.preventDefault();

        const value = e.target.value;

        this.setState({
            items: take(data.items.filter(item => {
                const f = item.family.toLowerCase();
                return f.indexOf(value.toLowerCase()) >= 0
            }), 20)
        });

    }

    render() {

        return (
            <Modal show={this.props.show} onHide={this.close}>
                <Modal.Header>
                    <Modal.Title>Modal heading</Modal.Title>
                    <Button onClick={this.props.closeModal}>Close</Button>
                </Modal.Header>
                <Modal.Body>

                    <div className="row">
                        <div className="col-xs-12">
                            <input type="text"

                                   onChange={this._onSearch}

                                   style={{
                                       display: 'block',
                                       width: '100%'
                                   }}/>
                        </div>
                    </div>

                    <hr/>

                    <div className="row">
                        {
                            this.state.items.map(font => <div key={font.family} className="col-xs-4">
                                <button className='btn btn-info ql-font'
                                        onClick={(e) => this.props.setFont(font)}>{font.family}</button>
                                <hr/>
                            </div>)
                        }
                    </div>

                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.props.closeModal}>Close</Button>
                </Modal.Footer>
            </Modal>
        )
    }
}

export default GoogleFont;